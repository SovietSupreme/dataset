#
#===============================================================================
#
#         FILE: Constants.pm
#
#      PROJECT: Customer Portal
#  DESCRIPTION: Contains application specific settings
#
#        FILES: ---
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Joseph Sloan (JS), joe.sloan@outlook.com
#      COMPANY: 
#    COPYRIGHT: Copyright (c) 2012
#      VERSION: 1.0
#      CREATED: 10/31/2012 07:14:26 PM
#     REVISION: ---
#===============================================================================

use strict;
use warnings;
 
package CeeBall::Constants;

use Exporter qw(import);
our @EXPORT_OK = qw(CONSTS);

use constant CONSTS => {
    DATABASE => {
        HOST            => "localhost",
        USERNAME        => "numberator",
        PASSWORD        => qq{!securepassword@#\$},
        DBNAME          => "numbers"
    },  
    PATHS => {
        TEMPLATES       => "/srv/portalicious/templates/"
    },    
};

1;
