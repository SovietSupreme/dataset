#
#===============================================================================
#
#         FILE: DataSet.pm
#
#      PROJECT: Customer Portal
#  DESCRIPTION: Provides a Database abstraction layer for mojolicious
#
#         BUGS: ---
#        NOTES: This is very similar to how modules like DBIX work, but with
#               a much more basic approach. It uses the same "chain"
#               like subroutine calls by building each part of the sql query
#               and storing it in the object, then returning the object reference 
#               each time, followed by an endpoint call.
#               
#       AUTHOR: Joseph Sloan (js), joe.sloan@outlook.com
#      COMPANY: 
#      VERSION: 1.0
#      CREATED: 10/10/2012 07:57:27 PM
#     REVISION: ---
#===============================================================================

package CeeBall::Database::DataSet;

use CeeBall::Database;

use strict;
use warnings;

use Data::Dumper;

our $DBC;

#===  FUNCTION  ================================================================
#         NAME: new
#      PURPOSE: constructor
#   PARAMETERS: name of database table / mojolicious controller
#      RETURNS: DataSet object reference
#  DESCRIPTION: spawns object and creates database connection
#       THROWS: 
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub new {
    my ( $class, $table, $controller ) = @_;
    my $self = {};
    $self->{TABLE} = $table;
    $self->{COLUMNS} = qq{ * };
	$self->{JOINS} = qq{};
	$self->{WHERE} = qq{};
	$self->{BINDS} = [];
	$self->{ORDER} = qq{};
	$self->{LIMIT} = qq{};
	if(defined($controller)) {
		$self->{C}     = $controller;
    	$DBC           = $controller->db;
	} else {
		$DBC	= CeeBall::Database->connect();
	}
    bless( $self, $class );
    return $self;
}


#===  FUNCTION  ================================================================
#         NAME: save
#      PURPOSE: inserts or updates records
#   PARAMETERS: hash ref of field names and values
#      RETURNS: primary key of dataset
#  DESCRIPTION: inserts a new record with specified fields passed in 
#               or updates an existing record if primary key is included
#       THROWS: exceptions related to the database
#     COMMENTS: none
#     SEE ALSO: n/a
#===============================================================================
sub save {
    my ( $self, $fields ) = @_;
    my $cols;
    my $sql;
    my $dh;
    my $error;
    my $pkey;
    my $pkeyval;
    my $colkey;
    my @columns;
    my @values;
    my @binds;
    my $value;

    if(!defined($self->{TABLE}) || !defined($fields)) {
		return;
	}

	if ( ref($fields) ne "HASH" ) {
        return;
    }

    foreach $colkey ( keys %{$fields} ) {
        if ( $colkey =~ /^\w{3}ID$/ ) {    # SKIP THE PRIMARY KEY
			$pkey = $colkey;
			$pkeyval = $fields->{$colkey};
            next;
        }

        push( @columns, $colkey );

        if ( ref $fields->{$colkey} eq "HASH" ) {    # EXTENDED COLUMN
            if ( $fields->{$colkey}->{BINDVALUE} ) {    # CUSTOM BIND VALUE
                push( @binds, $fields->{$colkey}->{BINDVALUE} );
            }
			elsif ( ref $fields->{$colkey}->{VALUE} eq "ARRAY" ) {
                push( @values, @{ $fields->{$colkey}->{VALUES} } );
            }
            else {
                push( @binds, "?" );
            }
        }
        else {    # STANDARD COLUMN
            push( @values, $fields->{$colkey} );
            push( @binds,  "?" );
        }
    }

    if ( defined($pkey) ) {    ##### PRIMARY KEY FOUND, PERFORM UPDATE
        $sql = "UPDATE " . $self->{TABLE} . " SET ";

        for ( my $i = 0 ; $i < @columns ; $i++ ) {
            $sql .= $columns[$i] . " = " . $binds[$i] . ",";
        }

        $sql =~ s/,$//gi;
        $sql .= " WHERE " . $pkey . " = " . $pkeyval;

        $DBC->nonquery($sql,\@values);
    }
    else {               ##### NO PRIMARY KEY, PERFORM INSERT
        $sql = "INSERT INTO " . $self->{TABLE} . " (";
        $sql .= join( ",", @columns );
        $sql .= ") VALUES (";
        $sql .= join( ",", @binds );
        $sql .= ")";
 
        $pkeyval = $DBC->nonquery($sql,\@values);
    }
    return $pkeyval;
}

sub select {
	my $self = shift;
	my $what;

	foreach my $param (@_) {
		if(ref($param) eq "ARRAY") {
			$what .= join(", ", @$param);
		}
		else {
			$what .= qq{ $param, };
		}
	}

	if(defined($what)) {
		$what =~ s/, $//;	
		$self->{COLUMNS} = $what;
	}

	return $self;
}


sub where {
	my($self, $params) = @_;
	
	if(defined($params)) {
		if(ref($params) eq "HASH") {
			$self->{WHERE} .= qq{ WHERE };
			foreach(keys %$params) {
				if(defined($params->{$_})) {
					if(ref($params->{$_}) eq "HASH") {
						$self->{WHERE} .= qq{ $_ = $params->{$_}->{BINDVALUE} AND};
					} else {
						$self->{WHERE} .= qq{ $_ = ? AND};
						push(@{$self->{BINDS}},$params->{$_});
					}
				} else {
					$self->{WHERE} .= qq{ $_ = '' AND};
				}
			}
			$self->{WHERE} =~ s/AND$//;
		}
	}

	return $self;
}

sub joins {
	my ($self,$joins) = @_;

	if(defined($joins)) {
		if(ref($joins) eq "ARRAY") {
			$self->{JOINS} .= join(" ",@$joins);
		} else {
			$self->{JOINS} .= $joins;
		}
	}	
	return $self;
}

sub order {
	my $self = shift;
	my $order;

	foreach my $param (@_) { 
		if(ref($param) eq "HASH") {
			foreach my $sortby (keys %{$param}) {
				$order .= qq{ $_[0]->{$sortby} $sortby, };			
			}
		} elsif(ref($param) eq "ARRAY") {
			$order .= join(", ",@$param);
		} else {
			$order .= qq{ $param, };
		}
	}

	if(defined($order)) {
		$order =~ s/, $//;
		$self->{ORDER} = qq{ ORDER BY $order };
	}

	return $self;
}

sub group {
	my ($self,$group) = @_;

		
}



#-------------------------------------------------------------------------------
#  Next 3 subroutines are "endpoint" calls
#-------------------------------------------------------------------------------


#===  FUNCTION  ================================================================
#         NAME: all
#      PURPOSE: Provides all rows from a given query
#   PARAMETERS: object reference
#      RETURNS: array reference of rows with hash references to fields and vals
#  DESCRIPTION: Constructs the entire query from the data given in the other 
#               dataset calls
#       THROWS: no exceptions
#     COMMENTS: *MUST* be called last in the chain
#     SEE ALSO: single,delete
#===============================================================================
sub all {
	my($self) = @_;

	if(!defined($DBC)) {
		return;
	}
	
	my $sql = qq{ SELECT $self->{COLUMNS} FROM $self->{TABLE} };
	
	if(length($self->{JOINS})) {
		$sql .= $self->{JOINS};
	}
	
	if(length($self->{WHERE})) {
		$sql .= $self->{WHERE};
	}

	if(length($self->{ORDER})) {
		$sql .= $self->{ORDER};
	}

	return $DBC->query($sql,$self->{BINDS});
}

#===  FUNCTION  ================================================================
#         NAME: single
#      PURPOSE: Provides a single row from a given query
#   PARAMETERS: object reference
#      RETURNS: hash reference of selected row columns and values
#  DESCRIPTION: Constructs the entire query from the data given in the other
#               dataset calls
#       THROWS: no exceptions
#     COMMENTS: *MUST* be called last in the chain
#     SEE ALSO: all,delete
#===============================================================================
sub single {
	my($self) = @_;

	my $sql = qq{ SELECT $self->{COLUMNS} FROM $self->{TABLE} };
	
	if(length($self->{JOINS})) {
		$sql .= $self->{JOINS};
	}
	
	if(length($self->{WHERE})) {
		$sql .= $self->{WHERE};
	}

	if(length($self->{ORDER})) {
		$sql .= $self->{ORDER};
	}

	$sql .= qq{ LIMIT 1 };

	my $arref = $DBC->query($sql,$self->{BINDS});
	
	if(ref($arref) eq "ARRAY") {
		return $arref->[0];
	} else {
		return {};
	}
}

#===  FUNCTION  ================================================================
#         NAME: delete
#      PURPOSE: Provides the ability to remove database rows
#   PARAMETERS: Object reference
#      RETURNS: ????
#  DESCRIPTION: Deletes row(s) from the database based on certain criteria
#       THROWS: no exceptions
#     COMMENTS: A WHERE clause is *REQUIRED*
#     SEE ALSO: n/a
#===============================================================================
sub delete {
	my($self) = @_;
	
	my $sql = qq{ DELETE FROM $self->{TABLE} };
	
	if(length($self->{WHERE})) {
		$sql .= $self->{WHERE};
		return $DBC->nonquery($sql,$self->{BINDS});	
	} else {
		## dont do it without a where clause
		return 0;
	}	
}


1;
