#
#===============================================================================
#
#         FILE: Database.pm
#
#      PROJECT: Customer Portal
#  DESCRIPTION: Provides database access layer for mojolicious
#
#         BUGS: ---
#        NOTES: ---
#       AUTHOR: Joseph Sloan (js), joe.sloan@outlook.com
#      COMPANY: 
#      VERSION: 1.0
#      CREATED: 10/05/2012 01:00:30 PM
#     REVISION: ---
#===============================================================================

package CeeBall::Database;

use DBI;
use Data::Dumper;
use CeeBall::Constants qw/CONSTS/;

use warnings;
use strict;

BEGIN { $ENV{DBIC_TRACE} = 0 }

our $showqueries = 1;

sub connect {
    my ( $class ) = @_;
    my $self = {};

	my $dsn = "DBI:mysql:database=<DATABASE>;host=localhost;port=3306";

	my $dbname = CONSTS->{DATABASE}->{DBNAME};
	$dsn =~ s/<DATABASE>/$dbname/i;

	$self->{DBCONN} = DBI->connect
	(
		$dsn,
		CONSTS->{DATABASE}->{USERNAME},
		CONSTS->{DATABASE}->{PASSWORD}
	) or die $DBI::errstr;

    bless( $self, $class );
    return $self;
}

sub connect_from_controller {
	my ($class, $controller) = @_;
	my $self = {};

	 my $dsn = "DBI:mysql:database=<DATABASE>;host=localhost;port=3306";
	
	my $dbname = CONSTS->{DATABASE}->{DBNAME};
	$dsn =~ s/<DATABASE>/$dbname/i;

	$self->{C}      = $controller;
	$self->{DBCONN} = DBI->connect
	(
		$dsn,
		CONSTS->{DATABASE}->{USERNAME},
        CONSTS->{DATABASE}->{PASSWORD}
    ) or $controller->handle_exception($DBI::errstr);

	bless( $self, $class );
	return $self;
}

sub kill {
    my ($self) = @_;
    $self->{DBCONN}->disconnect;
}

sub define {
    my ( $self, $query ) = @_;
    my $dbh = $self->{DBCONN}->prepare($query);
    return $dbh;
}

sub query {
    my ( $self, $query, $values ) = @_;
    my $error;

	if($showqueries && defined($self->{C})) {
		$self->{C}->app->log->debug("QUERY: " . $query);
		$self->{C}->app->log->debug("BINDS: " . Dumper($values));
	}

    my $dbh = $self->define($query);

    if ( ref($values) eq "ARRAY" ) {
        $dbh->execute(@$values) or $error = $dbh->{mysql_error};
    }
    elsif ( $values && $query =~ /\?/gi ) {
        $dbh->execute($values) or $error = $dbh->{mysql_error};
    }
    else {
        $dbh->execute or $error = $dbh->{mysql_error};
    }

    if($error) {
		if(defined($self->{C})) {
			$self->{C}->handle_exception($error);
		} else {
			die $error;
		}
	}

    my $arref = $dbh->fetchall_arrayref( {} );

    $dbh->finish;

    return $arref;
}

sub nonquery {
    my ( $self, $query, $values ) = @_;
    my $error;
	my $rv;

	if($showqueries && defined($self->{C})) {
		$self->{C}->app->log->debug("NONQUERY: " . $query);
		$self->{C}->app->log->debug("BINDS: " . Dumper($values));
	}

    my $dbh = $self->define($query);

    if ( ref($values) eq "ARRAY" ) {
        $dbh->execute(@$values) or $error = $dbh->{mysql_error};
    }
    elsif ( $values && $query =~ /\?/gi ) {
        $dbh->execute($values) or $error = $dbh->{mysql_error};
    }
    else {
        $dbh->execute or $error = $dbh->{mysql_error};
    }

    if($error) {
		if(defined($self->{C})) {
			$self->{C}->handle_exception($error);
		} else {
			die $error;
		}
	}

	if(defined($dbh->{mysql_insertid})) {
		$rv = $dbh->{mysql_insertid};
	}

    $dbh->finish;

	return $rv;
}

sub DESTROY {
	my ($self) = @_;

	if(defined($self->{DBCONN})) {
		$self->{DBCONN}->disconnect;
		$self->{DBCONN} = undef;
	}
}

1;
